import { Component } from "@angular/core";
import { handleOpenURL, AppURL } from 'nativescript-urlhandler';

import { BackendService } from "./shared/backend.service";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
})
export class AppComponent {
  $ngOnInit() {
    handleOpenURL((appURL: AppURL) => {
      console.log('Got the following appURL', appURL);
    });
  }
}
