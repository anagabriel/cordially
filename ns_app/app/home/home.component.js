"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var image_1 = require("tns-core-modules/ui/image");
var router_1 = require("nativescript-angular/router");
var camera = require("nativescript-camera");
// import { OCR, RetrieveTextResult } from "nativescript-ocr";
// import { ImageSource } from "image-source";
// import { Nfc } from "nativescript-nfc";
var user_service_1 = require("../shared/user.service");
var HomeComponent = /** @class */ (function () {
    // private ocr: OCR;
    // doRecognize(image: ImageSource): void {
    // this.ocr.retrieveText({
    //   image,
    //   // whitelist: "ABCDEF",     // you can include only certain characters in the result
    //   // blacklist: "0123456789", // .. or you can exclude certain characters from the result
    //   onProgress: (percentage: number) => {
    //     console.log(`Decoding progress: ${percentage}%`);
    //   }
    // }).then(
    // (result: RetrieveTextResult) => {
    //   // this.set(HelloWorldModel.BUSY_KEY, false);
    //   console.log(`Result: ${result.text}`);
    // }).catch((err: string) => {
    //   console.log(`Error: ${err}`);
    // });
    // }
    function HomeComponent(userService, routerExtensions) {
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        // this.ocr = new OCR();
    }
    HomeComponent.prototype.ngOnInit = function () {
        // let nfc = new Nfc();
        //
        // nfc.available().then((avail) => {
        //   console.log(avail ? "Yes" : "No");
        // });
    };
    HomeComponent.prototype.captureFront = function () {
        var _this = this;
        this.captureImage().then(function (image) {
            _this.frontImage = image;
            // this.doRecognize(image.imageSource);
        });
    };
    HomeComponent.prototype.captureBack = function () {
        var _this = this;
        this.captureImage().then(function (image) {
            _this.backImage = image;
            // this.doRecognize(image.imageSource);
        });
    };
    HomeComponent.prototype.captureImage = function () {
        camera.requestPermissions();
        return camera.takePicture().
            then(function (imageAsset) {
            console.log("Result is an image asset instance");
            var image = new image_1.Image();
            image.src = imageAsset;
            return image;
        }).catch(function (err) {
            var error = new Error("Error -> " + err.message);
            console.error(error);
            return error;
        });
    };
    HomeComponent.prototype.checkIn = function () {
        this.routerExtensions.navigate(["/checkin"], { clearHistory: true });
    };
    HomeComponent.prototype.logout = function () {
        this.userService.logout();
        this.routerExtensions.navigate(["/login"], { clearHistory: true });
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "app-home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ['./home.component.css']
        }),
        __metadata("design:paramtypes", [user_service_1.UserService, router_1.RouterExtensions])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxtREFBa0Q7QUFDbEQsc0RBQStEO0FBQy9ELDRDQUE4QztBQUM5Qyw4REFBOEQ7QUFDOUQsOENBQThDO0FBQzlDLDBDQUEwQztBQUUxQyx1REFBcUQ7QUFRckQ7SUFHRSxvQkFBb0I7SUFFcEIsMENBQTBDO0lBQ3hDLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gseUZBQXlGO0lBQ3pGLDRGQUE0RjtJQUM1RiwwQ0FBMEM7SUFDMUMsd0RBQXdEO0lBQ3hELE1BQU07SUFDTixXQUFXO0lBQ1gsb0NBQW9DO0lBQ3BDLGtEQUFrRDtJQUNsRCwyQ0FBMkM7SUFDM0MsOEJBQThCO0lBQzlCLGtDQUFrQztJQUNsQyxNQUFNO0lBQ1IsSUFBSTtJQUVKLHVCQUFvQixXQUF3QixFQUFVLGdCQUFrQztRQUFwRSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDdEYsd0JBQXdCO0lBQzFCLENBQUM7SUFFRCxnQ0FBUSxHQUFSO1FBQ0UsdUJBQXVCO1FBQ3ZCLEVBQUU7UUFDRixvQ0FBb0M7UUFDcEMsdUNBQXVDO1FBQ3ZDLE1BQU07SUFDUixDQUFDO0lBRUQsb0NBQVksR0FBWjtRQUFBLGlCQU1DO1FBTEMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQVk7WUFDcEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFFeEIsdUNBQXVDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELG1DQUFXLEdBQVg7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUFZO1lBQ3BDLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBRXZCLHVDQUF1QztRQUN6QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxvQ0FBWSxHQUFaO1FBQ0UsTUFBTSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFFNUIsT0FBTyxNQUFNLENBQUMsV0FBVyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxVQUFDLFVBQVU7WUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFDakQsSUFBTSxLQUFLLEdBQUcsSUFBSSxhQUFLLEVBQUUsQ0FBQztZQUMxQixLQUFLLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQztZQUN2QixPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLEdBQUc7WUFDWCxJQUFNLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25ELE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckIsT0FBTyxLQUFLLENBQUE7UUFDZCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwrQkFBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDhCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUF2RVUsYUFBYTtRQU56QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFVBQVU7WUFDcEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx1QkFBdUI7WUFDcEMsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7U0FDcEMsQ0FBQzt5Q0F1QmlDLDBCQUFXLEVBQTRCLHlCQUFnQjtPQXRCN0UsYUFBYSxDQXdFekI7SUFBRCxvQkFBQztDQUFBLEFBeEVELElBd0VDO0FBeEVZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgSW1hZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9pbWFnZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCAqIGFzIGNhbWVyYSBmcm9tIFwibmF0aXZlc2NyaXB0LWNhbWVyYVwiO1xuLy8gaW1wb3J0IHsgT0NSLCBSZXRyaWV2ZVRleHRSZXN1bHQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LW9jclwiO1xuLy8gaW1wb3J0IHsgSW1hZ2VTb3VyY2UgfSBmcm9tIFwiaW1hZ2Utc291cmNlXCI7XG4vLyBpbXBvcnQgeyBOZmMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LW5mY1wiO1xuXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci5zZXJ2aWNlXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJhcHAtaG9tZVwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL2hvbWUuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbJy4vaG9tZS5jb21wb25lbnQuY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGZyb250SW1hZ2U/OiBJbWFnZTtcbiAgYmFja0ltYWdlPzogSW1hZ2U7XG4gIC8vIHByaXZhdGUgb2NyOiBPQ1I7XG5cbiAgLy8gZG9SZWNvZ25pemUoaW1hZ2U6IEltYWdlU291cmNlKTogdm9pZCB7XG4gICAgLy8gdGhpcy5vY3IucmV0cmlldmVUZXh0KHtcbiAgICAvLyAgIGltYWdlLFxuICAgIC8vICAgLy8gd2hpdGVsaXN0OiBcIkFCQ0RFRlwiLCAgICAgLy8geW91IGNhbiBpbmNsdWRlIG9ubHkgY2VydGFpbiBjaGFyYWN0ZXJzIGluIHRoZSByZXN1bHRcbiAgICAvLyAgIC8vIGJsYWNrbGlzdDogXCIwMTIzNDU2Nzg5XCIsIC8vIC4uIG9yIHlvdSBjYW4gZXhjbHVkZSBjZXJ0YWluIGNoYXJhY3RlcnMgZnJvbSB0aGUgcmVzdWx0XG4gICAgLy8gICBvblByb2dyZXNzOiAocGVyY2VudGFnZTogbnVtYmVyKSA9PiB7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKGBEZWNvZGluZyBwcm9ncmVzczogJHtwZXJjZW50YWdlfSVgKTtcbiAgICAvLyAgIH1cbiAgICAvLyB9KS50aGVuKFxuICAgIC8vIChyZXN1bHQ6IFJldHJpZXZlVGV4dFJlc3VsdCkgPT4ge1xuICAgIC8vICAgLy8gdGhpcy5zZXQoSGVsbG9Xb3JsZE1vZGVsLkJVU1lfS0VZLCBmYWxzZSk7XG4gICAgLy8gICBjb25zb2xlLmxvZyhgUmVzdWx0OiAke3Jlc3VsdC50ZXh0fWApO1xuICAgIC8vIH0pLmNhdGNoKChlcnI6IHN0cmluZykgPT4ge1xuICAgIC8vICAgY29uc29sZS5sb2coYEVycm9yOiAke2Vycn1gKTtcbiAgICAvLyB9KTtcbiAgLy8gfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHtcbiAgICAvLyB0aGlzLm9jciA9IG5ldyBPQ1IoKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIC8vIGxldCBuZmMgPSBuZXcgTmZjKCk7XG4gICAgLy9cbiAgICAvLyBuZmMuYXZhaWxhYmxlKCkudGhlbigoYXZhaWwpID0+IHtcbiAgICAvLyAgIGNvbnNvbGUubG9nKGF2YWlsID8gXCJZZXNcIiA6IFwiTm9cIik7XG4gICAgLy8gfSk7XG4gIH1cblxuICBjYXB0dXJlRnJvbnQoKSB7XG4gICAgdGhpcy5jYXB0dXJlSW1hZ2UoKS50aGVuKChpbWFnZTogSW1hZ2UpID0+IHtcbiAgICAgIHRoaXMuZnJvbnRJbWFnZSA9IGltYWdlO1xuXG4gICAgICAvLyB0aGlzLmRvUmVjb2duaXplKGltYWdlLmltYWdlU291cmNlKTtcbiAgICB9KTtcbiAgfVxuICBjYXB0dXJlQmFjaygpIHtcbiAgICB0aGlzLmNhcHR1cmVJbWFnZSgpLnRoZW4oKGltYWdlOiBJbWFnZSkgPT4ge1xuICAgICAgdGhpcy5iYWNrSW1hZ2UgPSBpbWFnZTtcblxuICAgICAgLy8gdGhpcy5kb1JlY29nbml6ZShpbWFnZS5pbWFnZVNvdXJjZSk7XG4gICAgfSk7XG4gIH1cbiAgY2FwdHVyZUltYWdlKCk6IFByb21pc2U8SW1hZ2V8RXJyb3I+IHtcbiAgICBjYW1lcmEucmVxdWVzdFBlcm1pc3Npb25zKCk7XG5cbiAgICByZXR1cm4gY2FtZXJhLnRha2VQaWN0dXJlKCkuXG4gICAgICB0aGVuKChpbWFnZUFzc2V0KSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiUmVzdWx0IGlzIGFuIGltYWdlIGFzc2V0IGluc3RhbmNlXCIpO1xuICAgICAgICBjb25zdCBpbWFnZSA9IG5ldyBJbWFnZSgpO1xuICAgICAgICBpbWFnZS5zcmMgPSBpbWFnZUFzc2V0O1xuICAgICAgICByZXR1cm4gaW1hZ2U7XG4gICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgIGNvbnN0IGVycm9yID0gbmV3IEVycm9yKFwiRXJyb3IgLT4gXCIgKyBlcnIubWVzc2FnZSk7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgICByZXR1cm4gZXJyb3JcbiAgICAgIH0pO1xuICB9XG5cbiAgY2hlY2tJbigpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2NoZWNraW5cIl0sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xuICB9XG5cbiAgbG9nb3V0KCkge1xuICAgIHRoaXMudXNlclNlcnZpY2UubG9nb3V0KCk7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9sb2dpblwiXSwgeyBjbGVhckhpc3Rvcnk6IHRydWUgfSk7XG4gIH1cbn1cblxuXG4iXX0=