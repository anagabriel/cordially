import { Component, OnInit } from "@angular/core";
import { Image } from "tns-core-modules/ui/image";
import { RouterExtensions } from "nativescript-angular/router";
import * as camera from "nativescript-camera";
// import { OCR, RetrieveTextResult } from "nativescript-ocr";
// import { ImageSource } from "image-source";
// import { Nfc } from "nativescript-nfc";

import { UserService } from "../shared/user.service";

@Component({
  selector: "app-home",
  moduleId: module.id,
  templateUrl: "./home.component.html",
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  frontImage?: Image;
  backImage?: Image;
  // private ocr: OCR;

  // doRecognize(image: ImageSource): void {
    // this.ocr.retrieveText({
    //   image,
    //   // whitelist: "ABCDEF",     // you can include only certain characters in the result
    //   // blacklist: "0123456789", // .. or you can exclude certain characters from the result
    //   onProgress: (percentage: number) => {
    //     console.log(`Decoding progress: ${percentage}%`);
    //   }
    // }).then(
    // (result: RetrieveTextResult) => {
    //   // this.set(HelloWorldModel.BUSY_KEY, false);
    //   console.log(`Result: ${result.text}`);
    // }).catch((err: string) => {
    //   console.log(`Error: ${err}`);
    // });
  // }

  constructor(private userService: UserService, private routerExtensions: RouterExtensions) {
    // this.ocr = new OCR();
  }

  ngOnInit(): void {
    // let nfc = new Nfc();
    //
    // nfc.available().then((avail) => {
    //   console.log(avail ? "Yes" : "No");
    // });
  }

  captureFront() {
    this.captureImage().then((image: Image) => {
      this.frontImage = image;

      // this.doRecognize(image.imageSource);
    });
  }
  captureBack() {
    this.captureImage().then((image: Image) => {
      this.backImage = image;

      // this.doRecognize(image.imageSource);
    });
  }
  captureImage(): Promise<Image|Error> {
    camera.requestPermissions();

    return camera.takePicture().
      then((imageAsset) => {
        console.log("Result is an image asset instance");
        const image = new Image();
        image.src = imageAsset;
        return image;
      }).catch((err) => {
        const error = new Error("Error -> " + err.message);
        console.error(error);
        return error
      });
  }

  checkIn() {
    this.routerExtensions.navigate(["/checkin"], { clearHistory: true });
  }

  logout() {
    this.userService.logout();
    this.routerExtensions.navigate(["/login"], { clearHistory: true });
  }
}


