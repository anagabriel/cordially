"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_urlhandler_1 = require("nativescript-urlhandler");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.$ngOnInit = function () {
        nativescript_urlhandler_1.handleOpenURL(function (appURL) {
            console.log('Got the following appURL', appURL);
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "ns-app",
            templateUrl: "app.component.html",
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsbUVBQWdFO0FBUWhFO0lBQUE7SUFNQSxDQUFDO0lBTEMsZ0NBQVMsR0FBVDtRQUNFLHVDQUFhLENBQUMsVUFBQyxNQUFjO1lBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDbEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBTFUsWUFBWTtRQUp4QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFFBQVE7WUFDbEIsV0FBVyxFQUFFLG9CQUFvQjtTQUNwQyxDQUFDO09BQ1csWUFBWSxDQU14QjtJQUFELG1CQUFDO0NBQUEsQUFORCxJQU1DO0FBTlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgaGFuZGxlT3BlblVSTCwgQXBwVVJMIH0gZnJvbSAnbmF0aXZlc2NyaXB0LXVybGhhbmRsZXInO1xuXG5pbXBvcnQgeyBCYWNrZW5kU2VydmljZSB9IGZyb20gXCIuL3NoYXJlZC9iYWNrZW5kLnNlcnZpY2VcIjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtYXBwXCIsXG4gICAgdGVtcGxhdGVVcmw6IFwiYXBwLmNvbXBvbmVudC5odG1sXCIsXG59KVxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XG4gICRuZ09uSW5pdCgpIHtcbiAgICBoYW5kbGVPcGVuVVJMKChhcHBVUkw6IEFwcFVSTCkgPT4ge1xuICAgICAgY29uc29sZS5sb2coJ0dvdCB0aGUgZm9sbG93aW5nIGFwcFVSTCcsIGFwcFVSTCk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==