import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { CheckinRoutingModule } from "./checkin-routing.module";
import { CheckinComponent } from "./checkin.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        CheckinRoutingModule
    ],
    declarations: [
        CheckinComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class CheckinModule { }
