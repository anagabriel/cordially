import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

import { UserService } from "../shared/user.service";

@Component({
  selector: "app-checkin",
  moduleId: module.id,
  templateUrl: "./checkin.component.html",
  styleUrls: ['./checkin.component.css']
})
export class CheckinComponent implements OnInit {
  constructor(private userService: UserService, private routerExtensions: RouterExtensions) {}

  ngOnInit(): void {
  }

  checkOut() {
  }

  logout() {
    this.userService.logout();
    this.routerExtensions.navigate(["/login"], { clearHistory: true });
  }
}


